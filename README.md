# Rental Car Booking Website

A rental car booking website for customers to choose location, date, type of vehicle/model ,booking date, period of use and the website displays available options according to the requirements of customer to make a booking.



All files should be present in localhost directory of server to work.

Server and mysql should be started for webpage to work.

main.html is the home page of the website.

form2.php is the file for personal info.

insert.php inserts all info to database.

retrieve.php retrieves info from database and displays in table.

Values for database should be changed in insert.php and retrieve.php,(root,admin,password).

All images in the images folder should be present in the root directory.

select_options.js is linked with main.html.

