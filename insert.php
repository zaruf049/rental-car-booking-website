<?php
	session_start();
	$name = $_POST['name'];
	$age = $_POST['age'];
	$license = $_POST['license'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$address = $_POST['address1'];
	
	
	if(!empty($name)|| !empty($age)|| !empty($license)|| !empty($email)|| !empty($phone)||!empty($address)){

	$host="localhost";
	$dbUsername="root";
	$dbPassword="";
	$dbname="wtproject";
	$conn=new mysqli($host,$dbUsername,$dbPassword,$dbname);
	if(mysqli_connect_error()){
		die('Connect Error('.mysqli_connect_error().')'.mysqli_connect_error());
	} else {
	
		$INSERT="INSERT Into bookings (name,age,id,email,phone,address,loc,rdate,ddate,vtype,vmake,vmodel) values(?,?,?,?,?,?,?,?,?,?,?,?)";
		$stmt=$conn->prepare($INSERT);
		$stmt->bind_param("sissssssssss",$name,$age,$license,$email,$phone,$address,$_SESSION['loc'],$_SESSION['pdate'],$_SESSION['rdate'],$_SESSION['type'],$_SESSION['make'],$_SESSION['model']);
		$stmt->execute();
		
		$stmt->close();
		$conn->close();
	}


	} 

	


	else {
		echo "All fields are required";
		die();
	}

?>


<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 80%;
  font-size: 22px;
  font-weight: 500;
}

.center {
  margin-left: auto;
  margin-right: auto;
}

td, th {
  border: 2px solid #000;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

.btn{
  text-decoration: none;
  padding: 10px 30px;
  border-radius: 200px;
  text-align: center;
}

.btn-full{
  background-color: #d8d8d8;
  color: #000;
  text-align: center;
  font-weight: 500;
  font-size: 19px;
}

.text{
  text-align: center;
}

</style>
</head>
<body>
<div class="text">
<h1 style="font-size: 50px;">Booking Details</h1>
<br><br>
<table class="center">
  <tr>
    <th>Name</th>
    <th><?php echo $name; ?></th>
  </tr>
  
  <tr>
    <td>Age</td>
    <td><?php echo $age; ?></td>
  </tr>
  <tr>
    <td>License</td>
    <td><?php echo $license; ?></td>
  </tr>
  <tr>
    <td>Email</td>
    <td><?php echo $email; ?></td>
  </tr>
  <tr>
    <td>Phone No</td>
    <td><?php echo $phone; ?></td>
    
  </tr>
  <tr>
    <td>Adress</td>
    <td><?php echo $address; ?></td>
    
  </tr>
   <tr>
    <td>Pickup Location</td>
    <td><?php echo $_SESSION['loc']; ?></td>
    
  </tr>
   <tr>
    <td>Pickup Date</td>
    <td><?php echo $_SESSION['pdate']; ?></td>
    
  </tr>
   <tr>
    <td>Return Date</td>
    <td><?php echo $_SESSION['rdate']; ?></td>
    
  </tr>
   <tr>
    <td>Vehicle Type</td>
    <td><?php echo $_SESSION['type']; ?></td>
    
  </tr>
   <tr>
    <td>Make</td>
    <td><?php echo $_SESSION['make']; ?></td>
    
  </tr>
  <tr>
    <td>Model</td>
    <td><?php echo $_SESSION['model']; ?></td>
    
  </tr>
</table>
<br><br><br>
<form action="https://localhost/main.html">
    <input type="submit" class="btn btn-full" value="Home Page"/>
  </div>
</form>
</body>
</html>
