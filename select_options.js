
  
var vehicleObject = {
  "SUV": {
    "Audi": ["Q3", "Q5", "Q7"],
    "Toyota": ["Fortuner", "Innova", "Land Cruiser"],
    "Land Rover": ["Evoque", "Defender", "Discovery"]    
  },
  "Sedan": {
    "Honda": ["City", "Civic", "Accord"],
    "Maruti Suzuki": ["Swift", "WagonR", "Ertiga"]
  },
  "Electric": {
    "Tesla": ["Model S", "Model X", "Roadster"]
   
  }
  
}
window.onload = function() {
  var typeSel = document.getElementById("type");
  var makeSel = document.getElementById("make");
  var modelSel = document.getElementById("model");
  for (var x in vehicleObject) {
    typeSel.options[typeSel.options.length] = new Option(x, x);
  }
  typeSel.onchange = function() {
    //empty Chapters- and Topics- dropdowns
    modelSel.length = 1;
    makeSel.length = 1;
    //display correct values
    for (var y in vehicleObject[this.value]) {
      makeSel.options[makeSel.options.length] = new Option(y, y);
    }
  }
  makeSel.onchange = function() {
    //empty Chapters dropdown
    modelSel.length = 1;
    //display correct values
    var z = vehicleObject[typeSel.value][this.value];
    for (var i = 0; i < z.length; i++) {
      modelSel.options[modelSel.options.length] = new Option(z[i], z[i]);
    }
  }
}
