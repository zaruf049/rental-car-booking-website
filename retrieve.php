<?php
$license=$_POST['lid'];
$host="localhost";
$dbUsername="root";
$dbPassword="";
$dbname="wtproject";
$conn=new mysqli($host,$dbUsername,$dbPassword,$dbname);
if(mysqli_connect_error()){
die('Connect Error('.mysqli_connect_error().')'.mysqli_connect_error());} 
else {
        $result = mysqli_query($conn,"SELECT * FROM bookings WHERE id=$license");}
?>

<!DOCTYPE html>
<html>
 <head>
    <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 80%;
  font-size: 22px;
  font-weight: 500;
}

.center {
  margin-left: auto;
  margin-right: auto;
}

td, th {
  border: 2px solid #000;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

.btn{
  text-decoration: none;
  padding: 10px 30px;
  border-radius: 200px;
  text-align: center;
}

.btn-full{
  background-color: #d8d8d8;
  color: #000;
  text-align: center;
  font-weight: 500;
  font-size: 19px;
}

.text{
  text-align: center;
}
</style>
 
 </head>
<body>
<?php
if (mysqli_num_rows($result) > 0) {
$i=0;
while($row = mysqli_fetch_array($result)) {
?>
<div class="text" >
<h2 style="font-size: 50px;">Booking Details</h2>

<table class="center">
  <tr>
    <th>Name</th>
    <th><?php echo $row["name"]; ?></th>
  </tr>
  
  <tr>
    <td>Age</td>
    <td><?php echo $row["age"]; ?></td>
  </tr>
  <tr>
    <td>License</td>
    <td><?php echo $row["id"]; ?></td>
  </tr>
  <tr>
    <td>Email</td>
    <td><?php echo $row["email"]; ?></td>
  </tr>
  <tr>
    <td>Phone No</td>
    <td><?php echo $row["phone"]; ?></td>
    
  </tr>
  <tr>
    <td>Adress</td>
    <td><?php echo $row["address"]; ?></td>
    
  </tr>
   <tr>
    <td>Pickup Location</td>
    <td><?php echo $row['loc']; ?></td>
    
  </tr>
   <tr>
    <td>Pickup Date</td>
    <td><?php echo $row['rdate']; ?></td>
    
  </tr>
   <tr>
    <td>Return Date</td>
    <td><?php echo $row['ddate']; ?></td>
    
  </tr>
   <tr>
    <td>Vehicle Type</td>
    <td><?php echo $row['vtype']; ?></td>
    
  </tr>
   <tr>
    <td>Make</td>
    <td><?php echo $row['vmake']; ?></td>
    
  </tr>
  <tr>
    <td>Model</td>
    <td><?php echo $row['vmodel']; ?></td>
    
  </tr>
</table>
<br><br><br>
    
<?php
$i++;
}
?>
</table>
 <?php
}
else{
    echo "No result found";
}
?>
<form action="https://localhost/main.html">
    <input type="submit" class="btn btn-full" value="Home Page"  />
</div>
</form>
 </body>
</html>